import numpy
from numpy import linalg


###################################
### MPM Phi_ip Matrix Functions ###
###################################

def phiip_mpm(nodes,parts):
    X,IM = numpy.meshgrid(parts.X.flatten(),nodes.I.flatten())
    Y,JM = numpy.meshgrid(parts.Y.flatten(),nodes.J.flatten())
    fvals = nodes.shapef(IM,JM,X,Y)
    return fvals

def delx_phiip_mpm(nodes,parts):
    X,IM = numpy.meshgrid(parts.X.flatten(),nodes.I.flatten())
    Y,JM = numpy.meshgrid(parts.Y.flatten(),nodes.J.flatten())
    fvals = nodes.delx_shapef(IM,JM,X,Y)
    return fvals

def dely_phiip_mpm(nodes,parts):
    X,IM = numpy.meshgrid(parts.X.flatten(),nodes.I.flatten())
    Y,JM = numpy.meshgrid(parts.Y.flatten(),nodes.J.flatten())
    fvals = nodes.dely_shapef(IM,JM,X,Y)
    return fvals

####################################
### CPDI Phi_ip Matrix Functions ###
####################################

def phiip_cpdi(nodes,parts):

    COR_00x,IM = numpy.meshgrid(parts.COR00x.flatten(),nodes.I.flatten())
    COR_00y,IM = numpy.meshgrid(parts.COR00y.flatten(),nodes.I.flatten())

    COR_01x,IM = numpy.meshgrid(parts.COR01x.flatten(),nodes.I.flatten())
    COR_01y,IM = numpy.meshgrid(parts.COR01y.flatten(),nodes.I.flatten())

    COR_10x,JM = numpy.meshgrid(parts.COR10x.flatten(),nodes.J.flatten())
    COR_10y,JM = numpy.meshgrid(parts.COR10y.flatten(),nodes.J.flatten())

    COR_11x,JM = numpy.meshgrid(parts.COR11x.flatten(),nodes.J.flatten())
    COR_11y,JM = numpy.meshgrid(parts.COR11y.flatten(),nodes.J.flatten())

    fvals_00 = nodes.shapef(IM,JM,COR_00x,COR_00y)
    fvals_01 = nodes.shapef(IM,JM,COR_01x,COR_01y)
    fvals_10 = nodes.shapef(IM,JM,COR_10x,COR_10y)
    fvals_11 = nodes.shapef(IM,JM,COR_11x,COR_11y)

    return 0.25*(fvals_00 + fvals_01 + fvals_10 + fvals_11)


def cor00_phiip_cpdi(nodes,parts):
    X,IM = numpy.meshgrid(parts.COR00x.flatten(),nodes.I.flatten())
    Y,JM = numpy.meshgrid(parts.COR00y.flatten(),nodes.J.flatten())
    fvals = nodes.shapef(IM,JM,X,Y)
    return fvals

def cor01_phiip_cpdi(nodes,parts):
    X,IM = numpy.meshgrid(parts.COR01x.flatten(),nodes.I.flatten())
    Y,JM = numpy.meshgrid(parts.COR01y.flatten(),nodes.J.flatten())
    fvals = nodes.shapef(IM,JM,X,Y)
    return fvals

def cor10_phiip_cpdi(nodes,parts):
    X,IM = numpy.meshgrid(parts.COR10x.flatten(),nodes.I.flatten())
    Y,JM = numpy.meshgrid(parts.COR10y.flatten(),nodes.J.flatten())
    fvals = nodes.shapef(IM,JM,X,Y)
    return fvals

def cor11_phiip_cpdi(nodes,parts):
    X,IM = numpy.meshgrid(parts.COR11x.flatten(),nodes.I.flatten())
    Y,JM = numpy.meshgrid(parts.COR11y.flatten(),nodes.J.flatten())
    fvals = nodes.shapef(IM,JM,X,Y)
    return fvals



def delx_phiip_cpdi(nodes,parts):

    COR_00x,IM = numpy.meshgrid(parts.COR00x.flatten(),nodes.I.flatten())
    COR_00y,IM = numpy.meshgrid(parts.COR00y.flatten(),nodes.I.flatten())

    COR_01x,IM = numpy.meshgrid(parts.COR01x.flatten(),nodes.I.flatten())
    COR_01y,IM = numpy.meshgrid(parts.COR01y.flatten(),nodes.I.flatten())

    COR_10x,JM = numpy.meshgrid(parts.COR10x.flatten(),nodes.J.flatten())
    COR_10y,JM = numpy.meshgrid(parts.COR10y.flatten(),nodes.J.flatten())

    COR_11x,JM = numpy.meshgrid(parts.COR11x.flatten(),nodes.J.flatten())
    COR_11y,JM = numpy.meshgrid(parts.COR11y.flatten(),nodes.J.flatten())

    fvals_00 = nodes.delx_shapef(IM,JM,COR_00x,COR_00y)
    fvals_01 = nodes.delx_shapef(IM,JM,COR_01x,COR_01y)
    fvals_10 = nodes.delx_shapef(IM,JM,COR_10x,COR_10y)
    fvals_11 = nodes.delx_shapef(IM,JM,COR_11x,COR_11y)

    return 0.25*(fvals_00 + fvals_01 + fvals_10 + fvals_11)


def dely_phiip_cpdi(nodes,parts):

    COR_00x,IM = numpy.meshgrid(parts.COR00x.flatten(),nodes.I.flatten())
    COR_00y,IM = numpy.meshgrid(parts.COR00y.flatten(),nodes.I.flatten())

    COR_01x,IM = numpy.meshgrid(parts.COR01x.flatten(),nodes.I.flatten())
    COR_01y,IM = numpy.meshgrid(parts.COR01y.flatten(),nodes.I.flatten())

    COR_10x,JM = numpy.meshgrid(parts.COR10x.flatten(),nodes.J.flatten())
    COR_10y,JM = numpy.meshgrid(parts.COR10y.flatten(),nodes.J.flatten())

    COR_11x,JM = numpy.meshgrid(parts.COR11x.flatten(),nodes.J.flatten())
    COR_11y,JM = numpy.meshgrid(parts.COR11y.flatten(),nodes.J.flatten())

    fvals_00 = nodes.dely_shapef(IM,JM,COR_00x,COR_00y)
    fvals_01 = nodes.dely_shapef(IM,JM,COR_01x,COR_01y)
    fvals_10 = nodes.dely_shapef(IM,JM,COR_10x,COR_10y)
    fvals_11 = nodes.dely_shapef(IM,JM,COR_11x,COR_11y)

    return 0.25*(fvals_00 + fvals_01 + fvals_10 + fvals_11)


def printNodesTouchingParticle(PHI,nodes,parts,p):
    node_indices, = numpy.nonzero(PHI[:,p])
    for index in node_indices:
        print "---------------"
        print "Node Index:       ", index
        i = nodes.I.flatten()[index]
        j = nodes.J.flatten()[index]
        print "Node Row Index I: ", nodes.I.flatten()[index]
        print "Node Col Index J: ", nodes.J.flatten()[index]
        print "Node X,Y location:  ", nodes.X.flatten()[index],nodes.Y.flatten()[index]
        print "Node X,Y loc, again:", nodes.X[i,j],nodes.Y[i,j]


def matrixMultiply2x2(MAT1,MAT2):
    resultShape = MAT1[0][0].shape
    RESULT = ((numpy.zeros(resultShape),numpy.zeros(resultShape)),(numpy.zeros(resultShape),numpy.zeros(resultShape)))
    for i in xrange(0,2):
        for j in xrange(0,2):
            RESULT[i][j][:,:] = MAT1[i][0] * MAT2[0][j] + MAT1[i][1] * MAT2[1][j]
    return RESULT[0][0],RESULT[0][1],RESULT[1][0],RESULT[1][1]


def matrixMultiplyAinvB2x2(A,B):
    admbc = B[0][0]*B[1][1] - B[0][1]*B[1][0]
    invB = ( (  (1/admbc)*B[1][1],-(1/admbc)*B[0][1] ),\
             ( -(1/admbc)*B[1][0], (1/admbc)*B[0][0] ) )
    return matrixMultiply2x2(A,invB)


###############
### PLOTS #####
###############

def showMatrix(MAT,title="MAT"):
    import matplotlib
    matplotlib.interactive(True)
    from matplotlib import pyplot
    pyplot.figure()
    pyplot.imshow(MAT, interpolation="nearest")
    pyplot.title(title)
    pyplot.colorbar()

def showNodesAndParticles(nodes,parts,nind,pind,title="Domain",savefig=False,newfig=True,filename="fig.png",showCorners=True,ax=None):
    import matplotlib
    matplotlib.interactive(True)
    from matplotlib import pyplot
    if(ax==None):
        ax = pyplot
    if(newfig):
        pyplot.figure(figsize=(8,7.5))
    theplot = ax.plot(nodes.X[nind].flatten(),nodes.Y[nind].flatten(),"mx",label="Nodes")
    theplot = ax.plot(parts.X[pind].flatten(),parts.Y[pind].flatten(),"b*",label="Particle Centers")
    if(showCorners):
        theplot = ax.plot(parts.COR00x[pind].flatten(),parts.COR00y[pind].flatten(),"r.",label="Particle Corner 00")
        theplot = ax.plot(parts.COR01x[pind].flatten(),parts.COR01y[pind].flatten(),"g.",label="Particle Corner 01")
        theplot = ax.plot(parts.COR10x[pind].flatten(),parts.COR10y[pind].flatten(),"c.",label="Particle Corner 10")
        theplot = ax.plot(parts.COR11x[pind].flatten(),parts.COR11y[pind].flatten(),"y.",label="Particle Corner 11")
    if(ax==pyplot):
        ax.title(title)
    else:
        ax.set_title(title)
    xsize = nodes.xmax() - nodes.xmin()
    xmid = 0.5*(nodes.xmax() + nodes.xmin())
    ysize = nodes.ymax() - nodes.ymin()
    ymid = 0.5*(nodes.ymax() + nodes.ymin())
    biggersize = numpy.max([xsize,ysize])
    if(ax == pyplot):
        ax.xlim([xmid-biggersize/2,xmid+biggersize/2])
        ax.ylim([ymid-biggersize/2,ymid+biggersize/2])
    else:
        ax.set_xlim((xmid-biggersize/2,xmid+biggersize/2))
        ax.set_ylim((ymid-biggersize/2,ymid+biggersize/2))
    pyplot.legend()
    if(savefig):
        pyplot.savefig(filename)
        pyplot.close()
    return theplot

def plotNodesWithBC(nodes):
    import matplotlib
    matplotlib.interactive(True)
    from matplotlib import pyplot

    pyplot.figure(figsize=(8,7.5))
    pyplot.plot(nodes.X[nodes.pinned_x_bc_indices],nodes.Y[nodes.pinned_x_bc_indices],"g*")
    xsize = nodes.xmax() - nodes.xmin()
    xmid = 0.5*(nodes.xmax() + nodes.xmin())
    ysize = nodes.ymax() - nodes.ymin()
    ymid = 0.5*(nodes.ymax() + nodes.ymin())
    biggersize = numpy.max([xsize,ysize])
    pyplot.xlim([xmid-biggersize/2,xmid+biggersize/2])
    pyplot.ylim([ymid-biggersize/2,ymid+biggersize/2])
    pyplot.title("x_bc")

    pyplot.figure(figsize=(8,7.5))
    pyplot.plot(nodes.X[nodes.pinned_y_bc_indices],nodes.Y[nodes.pinned_y_bc_indices],"c*")
    xsize = nodes.xmax() - nodes.xmin()
    xmid = 0.5*(nodes.xmax() + nodes.xmin())
    ysize = nodes.ymax() - nodes.ymin()
    ymid = 0.5*(nodes.ymax() + nodes.ymin())
    biggersize = numpy.max([xsize,ysize])
    pyplot.xlim([xmid-biggersize/2,xmid+biggersize/2])
    pyplot.ylim([ymid-biggersize/2,ymid+biggersize/2])
    pyplot.title("y_bc")