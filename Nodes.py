# Python implementation of 2D MPM/CPDI
# Steven Schmidt
# January 2014

import numpy

class Nodes():

    def __init__(self,domainxmin,domainxmax,domainymin,domainymax,nnodesx,nnodesy):
        # Characteristics of the grid
        self.nnodesx = nnodesx
        self.nnodesy = nnodesy
        self.nnodes = nnodesx*nnodesy
        self.hx = (domainxmax - domainxmin)/(self.nnodesx-1)
        self.hy = (domainymax - domainymin)/(self.nnodesy-1)

        # one-dimensional arrays of x, y node locations
        self.x = numpy.linspace(domainxmin,domainxmax,self.nnodesx)
        self.y = numpy.linspace(domainymin,domainymax,self.nnodesy)

        # mesh-gridded matricies of x, y node locations
        self.X, self.Y = numpy.meshgrid(self.x,self.y)

        # mesh-gridded matricies of indices. "flattened" versions of these help map between flattened and
        # non-flattened versions of the data structures.
        self.J, self.I = numpy.meshgrid(numpy.arange(0,self.nnodesx,1),numpy.arange(0,self.nnodesy,1))

        # gridded matrices of values on grid, at each node location
        self.Qx = numpy.zeros((self.nnodesy,self.nnodesx))
        self.Qy = numpy.zeros((self.nnodesy,self.nnodesx))
        self.M = numpy.zeros((self.nnodesy,self.nnodesx))
        self.VOL = numpy.zeros((self.nnodesy,self.nnodesx))
        self.FINTx = numpy.zeros((self.nnodesy,self.nnodesx))
        self.FINTy = numpy.zeros((self.nnodesy,self.nnodesx))
        self.FEXTx = numpy.zeros((self.nnodesy,self.nnodesx))
        self.FEXTy = numpy.zeros((self.nnodesy,self.nnodesx))
        self.DQDTx = numpy.zeros((self.nnodesy,self.nnodesx))
        self.DQDTy = numpy.zeros((self.nnodesy,self.nnodesx))

        #Boundary condition information
        self.pinned_x_bc_indices = None
        self.pinned_y_bc_indices = None

    def xmin(self):
        return self.x[0]

    def xmax(self):
        return self.x[-1]

    def ymin(self):
        return self.y[0]

    def ymax(self):
        return self.y[-1]

    def shapef_common(self,i,j,X,Y):

        xdom0 = self.x[numpy.mod((j-1),self.nnodesx)]
        xdom1 = self.x[j]
        xdom2 = self.x[numpy.mod((j+1),self.nnodesx)]

        ydom0 = self.y[numpy.mod((i-1),self.nnodesy)]
        ydom1 = self.y[i]
        ydom2 = self.y[numpy.mod((i+1),self.nnodesy)]

        bool00 = numpy.logical_and(
                        numpy.logical_and(numpy.greater(X,xdom0), numpy.less(X,xdom1)),
                        numpy.logical_and(numpy.greater(Y,ydom0), numpy.less(Y,ydom1)))

        bool01 = numpy.logical_and(
                        numpy.logical_and(numpy.greater(X,xdom1), numpy.less(X,xdom2)),
                        numpy.logical_and(numpy.greater(Y,ydom0), numpy.less(Y,ydom1)))

        bool10 = numpy.logical_and(
                        numpy.logical_and(numpy.greater(X,xdom0), numpy.less(X,xdom1)),
                        numpy.logical_and(numpy.greater(Y,ydom1), numpy.less(Y,ydom2)))

        bool11 = numpy.logical_and(
                        numpy.logical_and(numpy.greater(X,xdom1), numpy.less(X,xdom2)),
                        numpy.logical_and(numpy.greater(Y,ydom1), numpy.less(Y,ydom2)))

        bool_lower_x = numpy.logical_and(numpy.logical_and(numpy.greater(X,xdom0), numpy.less(X,xdom1)), numpy.equal(Y,ydom1))
        bool_upper_x = numpy.logical_and(numpy.logical_and(numpy.greater(X,xdom1), numpy.less(X,xdom2)), numpy.equal(Y,ydom1))

        bool_lower_y = numpy.logical_and(numpy.logical_and(numpy.greater(Y,ydom0), numpy.less(Y,ydom1)), numpy.equal(X,xdom1))
        bool_upper_y = numpy.logical_and(numpy.logical_and(numpy.greater(Y,ydom1), numpy.less(Y,ydom2)), numpy.equal(X,xdom1))

        midpointbool = numpy.logical_and(numpy.equal(X,xdom1), numpy.equal(Y,ydom1))
        return xdom0,xdom1,xdom2,ydom0,ydom1,ydom2,bool00,bool01,bool10,bool11,bool_lower_x,bool_upper_x,bool_lower_y,bool_upper_y,midpointbool


    # Nodal basis function
    def shapef(self,i,j,X,Y):

        xdom0,xdom1,xdom2,ydom0,ydom1,ydom2,\
            bool00,bool01,bool10,bool11,\
            bool_lower_x,bool_upper_x,bool_lower_y,bool_upper_y,midpointbool = self.shapef_common(i,j,X,Y)

        return bool00*((X-xdom0)*(Y-ydom0))/((xdom1-xdom0)*(ydom1-ydom0))\
             + bool01*((X-xdom2)*(Y-ydom0))/((xdom1-xdom2)*(ydom1-ydom0))\
             + bool10*((X-xdom0)*(Y-ydom2))/((xdom1-xdom0)*(ydom1-ydom2))\
             + bool11*((X-xdom2)*(Y-ydom2))/((xdom1-xdom2)*(ydom1-ydom2))\
             + bool_lower_x*(X-xdom0)/(xdom1-xdom0)\
             + bool_upper_x*(X-xdom2)/(xdom1-xdom2)\
             + bool_lower_y*(Y-ydom0)/(ydom1-ydom0)\
             + bool_upper_y*(Y-ydom2)/(ydom1-ydom2)\
             + midpointbool*1.0

    # d/dx of nodal basis function
    def delx_shapef(self,i,j,X,Y):

        xdom0,xdom1,xdom2,ydom0,ydom1,ydom2,\
            bool00,bool01,bool10,bool11,\
            bool_lower_x,bool_upper_x,bool_lower_y,bool_upper_y,midpointbool = self.shapef_common(i,j,X,Y)

        return bool00*(Y-ydom0)/((-xdom0+xdom1)*(-ydom0+ydom1))\
             + bool01*(Y-ydom0)/((xdom1-xdom2)*(-ydom0+ydom1))\
             + bool10*(Y-ydom2)/((-xdom0+xdom1)*(ydom1-ydom2))\
             + bool11*(Y-ydom2)/((xdom1-xdom2)*(ydom1-ydom2))\
             + bool_lower_x*1/(-xdom0 + xdom1)\
             + bool_upper_x*1/(xdom1 - xdom2)\
             + bool_lower_y*0\
             + bool_upper_y*0\
             + midpointbool*0

    # d/dy of nodal basis function
    def dely_shapef(self,i,j,X,Y):

        xdom0,xdom1,xdom2,ydom0,ydom1,ydom2,\
            bool00,bool01,bool10,bool11,\
            bool_lower_x,bool_upper_x,bool_lower_y,bool_upper_y,midpointbool = self.shapef_common(i,j,X,Y)

        return bool00*(X-xdom0)/((-xdom0+xdom1)*(-ydom0+ydom1))\
             + bool01*(X - xdom2)/((xdom1 - xdom2)*(-ydom0 + ydom1))\
             + bool10*(X - xdom0)/((-xdom0 + xdom1)*(ydom1 - ydom2))\
             + bool11*(X-xdom2)/((xdom1-xdom2)*(ydom1-ydom2))\
             + bool_lower_x*0\
             + bool_upper_x*0\
             + bool_lower_y*1/(-ydom0+ydom1)\
             + bool_upper_y*1/(ydom1 - ydom2)\
             + midpointbool*0

    def plotNodalShapeFunction(self,i,j,type="regular"):
        import matplotlib
        matplotlib.interactive(True)
        from matplotlib import pyplot
        fig = pyplot.figure(figsize=(8,6))
        ax = fig.add_subplot(1, 1, 1, projection='3d')

        x = numpy.linspace(self.xmin(),self.xmax(),101*self.hx)
        y = numpy.linspace(self.ymin(),self.ymax(),101*self.hy)
        X, Y = numpy.meshgrid(x,y)
        if(type=="regular"):
            Z = self.shapef(i,j,X,Y)
        elif(type=="delx"):
            Z = self.delx_shapef(i,j,X,Y)
        elif(type=="dely"):
            Z = self.dely_shapef(i,j,X,Y)
        p = ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2)
        pyplot.title(type+" shapef "+str(i)+","+str(j))

    def plotField(self,FIELD,nonzero,title="Field",type="wireframe",resolutionPerCell=5,savefig=False,filename="fig.png"):
        import matplotlib
        matplotlib.interactive(True)
        from matplotlib import pyplot
        fig = pyplot.figure(figsize=(8,6))
        if(type=="wireframe"):
            ax = fig.add_subplot(1, 1, 1, projection='3d')
        elif(type=="pcolor"):
            ax = fig.add_subplot(1, 1, 1)
        else:
            print "ERROR Plot1303934"

        x = numpy.linspace(self.xmin(),self.xmax(),resolutionPerCell*(self.nnodesx-1)+1)
        y = numpy.linspace(self.ymin(),self.ymax(),resolutionPerCell*(self.nnodesx-1)+1)
        X, Y = numpy.meshgrid(x,y)
        Z = 0
        for index in xrange(0,nonzero[0].size):
            i = nonzero[0][index]
            j = nonzero[1][index]
            Z += FIELD[i,j]*self.shapef(i,j,X,Y)

        if(type=="wireframe"):
            p = ax.plot_wireframe(X, Y, Z, rstride=1, cstride=1)
        elif(type=="pcolor"):
            p = ax.pcolor(X, Y, Z)
            fig.colorbar(p, ax=ax)
        else:
            print "ERROR Plot2303934"

        pyplot.title(title)

        if(savefig):
            pyplot.savefig(filename)
            pyplot.close()

        return Z
