import numpy
from cpdipy2dhelper import *

A00 = 1*numpy.ones((2,2))
A01 = 2*numpy.ones((2,2))
A10 = 3*numpy.ones((2,2))
A11 = 4*numpy.ones((2,2))

B00 = 3*numpy.ones((2,2))
B01 = 4*numpy.ones((2,2))
B10 = 6*numpy.ones((2,2))
B11 = 7*numpy.ones((2,2))

R00 = numpy.zeros((2,2))
R01 = numpy.zeros((2,2))
R10 = numpy.zeros((2,2))
R11 = numpy.zeros((2,2))

Atest = numpy.array([[1,2],[3,4]])
Btest = numpy.array([[3,4],[6,7]])

R00[:,:], R01[:,:], R10[:,:], R11[:,:] = matrixMultiply2x2( ((A00,A01),(A10,A11)), ((B00,B01),(B10,B11)) )
print "matrixMultiply2x2(MAT1,MAT2): ", R00, R01, R10, R11

print "Atest.dot(Btest): ", Atest.dot(Btest)

R00[:,:], R01[:,:], R10[:,:], R11[:,:] = matrixMultiplyAinvB2x2( ((A00,A01),(A10,A11)), ((B00,B01),(B10,B11)) )
print "matrixMultiplyAinvB2x2(A,B): ", R00, R01, R10, R11

print "Atest.dot(numpy.linalg.inv(Btest)):", Atest.dot(numpy.linalg.inv(Btest))