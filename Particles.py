# Python implementation of 2D MPM/CPDI
# Steven Schmidt
# January 2014

import numpy

class Particles():

    def __init__(self,partsxmin,partsxmax,partsymin,partsymax,npartsx,npartsy):
        self.npartsx = npartsx
        self.npartsy = npartsy
        self.nparts = npartsx*npartsy
        self.restlengthx = (partsxmax - partsxmin)/npartsx
        self.restlengthy = (partsymax - partsymin)/npartsy

        # particle centers
        xcenters = numpy.linspace(partsxmin+(self.restlengthx/2), partsxmax-(self.restlengthx/2), self.npartsx)
        ycenters = numpy.linspace(partsymin+(self.restlengthy/2), partsymax-(self.restlengthy/2), self.npartsy)
        self.X,self.Y = numpy.meshgrid(xcenters,ycenters)

        # Mesh-grid of indicies
        self.J, self.I = numpy.meshgrid(numpy.arange(0,self.npartsx,1),numpy.arange(0,self.npartsy,1))

        # Particle corner locations
        corner00x = numpy.linspace(partsxmin,                  partsxmax-self.restlengthx,self.npartsx)
        corner00y = numpy.linspace(partsymin,                  partsymax-self.restlengthy,self.npartsy)
        self.COR00x, self.COR00y = numpy.meshgrid(corner00x,corner00y)

        corner01x = numpy.linspace(partsxmin+self.restlengthx, partsxmax,                 self.npartsx)
        corner01y = numpy.linspace(partsymin,                  partsymax-self.restlengthy,self.npartsy)
        self.COR01x, self.COR01y = numpy.meshgrid(corner01x,corner01y)

        corner10x = numpy.linspace(partsxmin,                  partsxmax-self.restlengthx,self.npartsx)
        corner10y = numpy.linspace(partsymin+self.restlengthy, partsymax,                 self.npartsy)
        self.COR10x, self.COR10y = numpy.meshgrid(corner10x,corner10y)

        corner11x = numpy.linspace(partsxmin+self.restlengthx, partsxmax,                 self.npartsx)
        corner11y = numpy.linspace(partsymin+self.restlengthy, partsymax,                 self.npartsy)
        self.COR11x, self.COR11y = numpy.meshgrid(corner11x,corner11y)

        # Rest matrix, represnting the rest configuration of the particle
        # defined by the points: center, middle-of-right-side, and middle-of-top
        restp00x = self.X
        restp00y = self.Y
        restp01x = 0.5*(self.COR01x + self.COR11x)
        restp01y = 0.5*(self.COR01y + self.COR11y)
        restp10x = 0.5*(self.COR10x + self.COR11x)
        restp10y = 0.5*(self.COR10y + self.COR11y)
        self.REST00 = restp01x-restp00x
        self.REST01 = restp01y-restp00y
        self.REST10 = restp10x-restp00x
        self.REST11 = restp10y-restp00y

        self.DET_REST = self.REST00*self.REST11 - self.REST01*self.REST10

        # Storing each of the four parts of the defgrad matrix each as separate arrays (index specifies the particle)
        self.DEFGRAD00 = numpy.ones((self.npartsy,self.npartsx))
        self.DEFGRAD01 = numpy.zeros((self.npartsy,self.npartsx))
        self.DEFGRAD10 = numpy.zeros((self.npartsy,self.npartsx))
        self.DEFGRAD11 = numpy.ones((self.npartsy,self.npartsx))

        #Determinant of Deformation Gradiant
        self.DET_DEFGRAD = numpy.zeros((self.npartsy,self.npartsx))

        # Other particle quantities
        # particle momentum
        self.Qx = numpy.zeros((self.npartsy,self.npartsx))
        self.Qy = numpy.zeros((self.npartsy,self.npartsx))

        # particle mass
        self.M = numpy.zeros((self.npartsy,self.npartsx))

        # particle volume
        self.VOL = numpy.abs((self.COR01x - self.COR00x) * (self.COR10y - self.COR00y)).copy()

        # particle "rest" volume
        self.RESTVOL = self.VOL.copy()

        self.STRESS_00 = numpy.zeros((self.npartsy,self.npartsx))
        self.STRESS_01 = numpy.zeros((self.npartsy,self.npartsx))
        self.STRESS_10 = numpy.zeros((self.npartsy,self.npartsx))
        self.STRESS_11 = numpy.zeros((self.npartsy,self.npartsx))

        self.FEXTx = numpy.zeros((self.npartsy,self.npartsx))
        self.FEXTy = numpy.zeros((self.npartsy,self.npartsx))

        self.DQDTx = numpy.zeros((self.npartsy,self.npartsx))
        self.DQDTy = numpy.zeros((self.npartsy,self.npartsx))

        self.DELxVx00 = numpy.zeros((self.npartsy,self.npartsx))  # velocity gradient - d/dx of Vx
        self.DELyVx01 = numpy.zeros((self.npartsy,self.npartsx))  # velocity gradient - d/dy of Vx
        self.DELxVy10 = numpy.zeros((self.npartsy,self.npartsx))  # velocity gradient - d/dx of Vy
        self.DELyVy11 = numpy.zeros((self.npartsy,self.npartsx))  # velocity gradient - d/dy of Vy


    # xmin of full domain of the particles
    def xmin(self):
        return self.X.min()

    # xmax of full domain of the particles
    def xmax(self):
        return self.X.max()

    def ymin(self):
        return self.Y.min()

    def ymax(self):
        return self.Y.max()
        
    def calc_det_defgrad(self,indices):
        return self.DEFGRAD00[indices]*self.DEFGRAD11[indices] - self.DEFGRAD01[indices]*self.DEFGRAD10[indices]



