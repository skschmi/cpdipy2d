#!/opt/local/bin/python -i

################################
### PYTHON IMPORT STATEMENTS ###
################################

import numpy
from numpy import linalg
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot
from pylab import *
from mpl_toolkits.mplot3d.axes3d import Axes3D
import Nodes
import Particles
from cpdipy2dhelper import *
import matplotlib.animation as animation

######################
### GENERAL SETUP: ###
######################

TYPE_MPM    = "MPM"
TYPE_CPDI1  = "CPDI-1"
TYPE_CPDI2  = "CPDI-2"
MAP_REGULAR = "Regular Mapping"
MAP_PINV    = "Pseudo Inverse Mapping"

smallvaluelimit = 1e-14

################################
### Initialization of Nodes: ###
################################

spacexmin = 0.0
spacexmax = 2.0
spaceymin = 0.0
spaceymax = 2.0
space_ncells_x = 20
space_ncells_y = 20
nodes_hx = (spacexmax-spacexmin)/space_ncells_x
nodes_hy = (spaceymax-spaceymin)/space_ncells_y

nodes = Nodes.Nodes(domainxmin=spacexmin-nodes_hx,
                    domainxmax=spacexmax+nodes_hx,
                    domainymin=spaceymin-nodes_hy,
                    domainymax=spaceymax+nodes_hy,
                    nnodesx=space_ncells_x+1+2,
                    nnodesy=space_ncells_y+1+2)

nodes.pinned_x_bc_indices = numpy.where(numpy.logical_or(nodes.X <= nodes.xmin()+nodes.hx, nodes.X >= nodes.xmax()-nodes.hx))
nodes.pinned_y_bc_indices = numpy.where(numpy.logical_or(nodes.Y <= nodes.ymin()+nodes.hy, nodes.Y >= nodes.ymax()-nodes.hy))

#plotNodesWithBC(nodes)


####################################
### Initialization of Particles: ###
####################################

parts = Particles.Particles(partsxmin = nodes.xmin()+nodes.hx+1.5,
                            partsxmax = nodes.xmax()-nodes.hx,
                            partsymin = nodes.ymin()+nodes.hy,
                            partsymax = nodes.ymin()+nodes.hy+0.7,
                            npartsx = 5*2,
                            npartsy = 7*2)

waterdensity = 1e3 # kg/m^3
parts.M[:,:] = parts.VOL[:,:]*waterdensity
parts.FEXTy[:,:] = parts.M*(-9.81)


###########################
### Material Constants: ###
###########################

# Tait's constant:
C_Tait = 0.0894

# Bulk Modulus:
K_bulkMod = 15e3 # unit: Pa

# Shear Viscosity
mu_shear = 500e-2 # unit: Poise (500 cP)

# Note:
# Two viscosities: dynamic viscosity, and kinimatic viscosity (one of them is the other times the density)
# Jim Guilkey using:  mu_shear = 500e-2 Poise (500 cP), and K_bulkMod = 15e3....15e6 Pa
# Water: mu_shear = 1.0e-3 Poise (1.0 cP), and K_bulkMod = 2.2e9 Pa

###################
### Time Step #####
###################

def calcTimeStep(nodes,parts):
    # Timestep should be: 0.2 times the time for wave to go across one cell at wave speed sqrt(E/rho)
    hcell = numpy.max([nodes.hx,nodes.hy])
    density = parts.M[0,0]/parts.VOL[0,0]
    return 0.025*hcell/sqrt(K_bulkMod/density)

dt = calcTimeStep(nodes,parts)
currenttime = 0.0

#########################
### SIMULATION SETUP: ###
#########################

maxsteps = 400 #int(numpy.ceil(1.0/dt))

algorithm_type = TYPE_MPM
mapping_type = MAP_REGULAR
#mapping_type = MAP_PINV

ANIMATE = True
SAVEFIG = False

#######################
## ANIMATION SETUP ####
#######################

stepsPerFrame = 20
framesPerSecond = 15
moviefilename = "animation/movie.mp4"
numFrames = int(numpy.ceil(float(maxsteps)/stepsPerFrame))


###########################
### Plot Initial State: ###
###########################

showNodesAndParticles(nodes,parts,numpy.where(nodes.X),numpy.where(parts.X),title="Initial Domain Configuration, t=0")


######################
### Algorithm Loop ###
######################


PHI = None
DELPHIx = None
DELPHIy = None
nodesshape = (nodes.nnodesy,nodes.nnodesx)
partsshape = (parts.npartsy,parts.npartsx)

def algorithmStep(step):

    global PHI, DELPHIx, DELPHIy, TYPE_MPM, TYPE_CPDI1, TYPE_CPDI2, MAP_REGULAR, MAP_PINV,\
        maxsteps, algorithm_type, mapping_type, nodes, parts,\
        lamb, mu, dt, nodesshape, partsshape, currenttime

    print "Step: ", step, "/", maxsteps, ("   t = %2.6f" % (currenttime)), " s"
    currenttime += dt

    #----------------------------
    #Calculate the PHI_ip matrix
    #----------------------------

    if(algorithm_type == TYPE_MPM):
        PHI = phiip_mpm(nodes,parts)
        DELPHIx = delx_phiip_mpm(nodes,parts)
        DELPHIy = dely_phiip_mpm(nodes,parts)
    elif(algorithm_type == TYPE_CPDI1 or algorithm_type == TYPE_CPDI2):
        PHI = phiip_cpdi(nodes,parts)
        DELPHIx = delx_phiip_cpdi(nodes,parts)
        DELPHIy = dely_phiip_cpdi(nodes,parts)
    else:
        print "ERROR 64021 INVALID MPM TYPE"
        quit()

    # Check for small values
    ind = numpy.where(numpy.abs(PHI)<smallvaluelimit)
    PHI[ind] = 0.0
    ind = numpy.where(numpy.abs(DELPHIx)<smallvaluelimit)
    DELPHIx[ind] = 0.0
    ind = numpy.where(numpy.abs(DELPHIy)<smallvaluelimit)
    DELPHIy[ind] = 0.0

    if(mapping_type == MAP_REGULAR):
        PHI_pton = PHI
        DELPHIx_pton = DELPHIx
        DELPHIy_pton = DELPHIy
    elif(mapping_type == MAP_PINV):
        PHI_pton = linalg.pinv(PHI.dot(PHI.T)).dot(PHI)
        DELPHIx_pton = linalg.pinv(DELPHIx.dot(DELPHIx.T)).dot(DELPHIx)
        DELPHIy_pton = linalg.pinv(DELPHIy.dot(DELPHIy.T)).dot(DELPHIy)
    else:
        print "ERROR 33234 INVALID MAPPING TYPE"
        quit()

    # Again check for small values
    ind = numpy.where(numpy.abs(PHI_pton)<smallvaluelimit)
    PHI_pton[ind] = 0.0
    ind = numpy.where(numpy.abs(DELPHIx_pton)<smallvaluelimit)
    DELPHIx_pton[ind] = 0.0
    ind = numpy.where(numpy.abs(DELPHIy_pton)<smallvaluelimit)
    DELPHIy_pton[ind] = 0.0

    #----------------------------
    # Map from Particles to Nodes
    #----------------------------

    # Map Mass (Equation 1)
    nodes.M[:,:] = PHI_pton.dot(parts.M.flatten()).reshape(nodesshape)


    # Map Momentum (Equation 2)
    nodes.Qx[:,:] = PHI_pton.dot(parts.Qx.flatten()).reshape(nodesshape)
    nodes.Qy[:,:] = PHI_pton.dot(parts.Qy.flatten()).reshape(nodesshape)


    # Map Volume
    nodes.VOL[:,:] = PHI_pton.dot(parts.VOL.flatten()).reshape(nodesshape)


    # Map External Force
    nodes.FEXTx[:,:] = PHI_pton.dot(parts.FEXTx.flatten()).reshape(nodesshape)
    nodes.FEXTy[:,:] = PHI_pton.dot(parts.FEXTy.flatten()).reshape(nodesshape)


    # Map Internal Force (Equation 8)
    nodes.FINTx[:,:] = -(DELPHIx_pton.dot((parts.STRESS_00 * parts.VOL).flatten()).reshape(nodesshape) \
                     + DELPHIy_pton.dot((parts.STRESS_10 * parts.VOL).flatten()).reshape(nodesshape))
    nodes.FINTy[:,:] = -(DELPHIx_pton.dot((parts.STRESS_01 * parts.VOL).flatten()).reshape(nodesshape) \
                     + DELPHIy_pton.dot((parts.STRESS_11 * parts.VOL).flatten()).reshape(nodesshape))

    #-------------------------------
    # Set "small mass" nodes to zero
    #-------------------------------

    smallmi = numpy.where(numpy.abs(nodes.M) <= smallvaluelimit)
    nodes.M[smallmi] = 0.0
    nodes.Qx[smallmi] = 0.0
    nodes.Qy[smallmi] = 0.0
    nodes.VOL[smallmi] = 0.0
    nodes.FEXTx[smallmi] = 0.0
    nodes.FEXTy[smallmi] = 0.0
    nodes.FINTx[smallmi] = 0.0
    nodes.FINTy[smallmi] = 0.0

    #----------
    # Set dQ/dt
    #----------

    # dQ/dt (or Acceleration * mass) (Equation 9)
    nodes.DQDTx[:,:] = nodes.FINTx + nodes.FEXTx
    nodes.DQDTy[:,:] = nodes.FINTy + nodes.FEXTy

    #-----------
    # Move Nodes
    #-----------

    nodes.Qx[:,:] = nodes.Qx + nodes.DQDTx*dt
    nodes.Qy[:,:] = nodes.Qy + nodes.DQDTy*dt

    #-----------------------------------
    # Apply Boundary Conditions on nodes
    #-----------------------------------

    if(nodes.pinned_x_bc_indices != None):
        nodes.Qx[nodes.pinned_x_bc_indices] = 0.0
        nodes.DQDTx[nodes.pinned_x_bc_indices] = 0.0

    if(nodes.pinned_y_bc_indices != None):
        nodes.Qy[nodes.pinned_y_bc_indices] = 0.0
        nodes.DQDTy[nodes.pinned_y_bc_indices] = 0.0

    #----------------------------
    # Map from Nodes to Particles
    #----------------------------

    # Calculating the velocities of nonzero-mass nodes ("nzm" means "nonzero mass")
    nzmi, = numpy.where(numpy.abs(nodes.M.flatten())>smallvaluelimit)
    nzmNodeVelx = (nodes.Qx.flatten()[nzmi]/nodes.M.flatten()[nzmi])
    nzmNodeVely = (nodes.Qy.flatten()[nzmi]/nodes.M.flatten()[nzmi])

    # Calculating Grad-V on the Particles (Equation 11)
    parts.DELxVx00[:,:] = (DELPHIx[nzmi,:].T).dot(nzmNodeVelx).reshape(partsshape)
    parts.DELyVx01[:,:] = (DELPHIy[nzmi,:].T).dot(nzmNodeVelx).reshape(partsshape)
    parts.DELxVy10[:,:] = (DELPHIx[nzmi,:].T).dot(nzmNodeVely).reshape(partsshape)
    parts.DELyVy11[:,:] = (DELPHIy[nzmi,:].T).dot(nzmNodeVely).reshape(partsshape)

    # Updating Particle Momentums (Equation 12)
    parts.Qx[:,:] = parts.Qx + parts.M * (PHI[nzmi,:].T).dot(nodes.DQDTx.flatten()[nzmi]/nodes.M.flatten()[nzmi]).reshape(partsshape) * dt
    parts.Qy[:,:] = parts.Qy + parts.M * (PHI[nzmi,:].T).dot(nodes.DQDTy.flatten()[nzmi]/nodes.M.flatten()[nzmi]).reshape(partsshape) * dt

    if(algorithm_type == TYPE_MPM or algorithm_type == TYPE_CPDI1):

        # Updating Particle Positions (Equation 13)
        parts.X[:] = parts.X + (PHI[nzmi,:].T).dot(nzmNodeVelx).reshape(partsshape)*dt
        parts.Y[:] = parts.Y + (PHI[nzmi,:].T).dot(nzmNodeVely).reshape(partsshape)*dt

        # Updating Deformation Gradient (Equation 14)
        IplusGradVdt = ((1 + parts.DELxVx00*dt, 0 + parts.DELyVx01*dt),\
                        (0 + parts.DELxVy10*dt, 1 + parts.DELyVy11*dt))

        olddefgrad = ((parts.DEFGRAD00, parts.DEFGRAD01),\
                      (parts.DEFGRAD10, parts.DEFGRAD11))
        parts.DEFGRAD00[:,:], parts.DEFGRAD01[:,:], parts.DEFGRAD10[:,:], parts.DEFGRAD11[:,:] = matrixMultiply2x2(IplusGradVdt,olddefgrad)

        # Moving the particle corners, based on their deformation gradient
        # STRETCHED = DEFGRAD * REST
        STRETCHED00x, STRETCHED01x, STRETCHED10y, STRETCHED11y = \
                        matrixMultiply2x2(  ((parts.DEFGRAD00, parts.DEFGRAD01),(parts.DEFGRAD10,parts.DEFGRAD11)),\
                                            ((parts.REST00,    parts.REST01   ),(parts.REST10,   parts.REST11   )) )

        parts.COR00x[:,:] = parts.X - STRETCHED00x - STRETCHED01x
        parts.COR00y[:,:] = parts.Y - STRETCHED10y - STRETCHED11y
        parts.COR01x[:,:] = parts.X + STRETCHED00x - STRETCHED01x
        parts.COR01y[:,:] = parts.Y + STRETCHED10y - STRETCHED11y
        parts.COR10x[:,:] = parts.X - STRETCHED00x + STRETCHED01x
        parts.COR10y[:,:] = parts.Y - STRETCHED10y + STRETCHED11y
        parts.COR11x[:,:] = parts.X + STRETCHED00x + STRETCHED01x
        parts.COR11y[:,:] = parts.Y + STRETCHED10y + STRETCHED11y
        del STRETCHED00x
        del STRETCHED01x
        del STRETCHED10y
        del STRETCHED11y

        # Calculating the determinant of the deformation gradient
        parts.DET_DEFGRAD[:,:] = parts.calc_det_defgrad(numpy.where(parts.X)).reshape(partsshape)

        # Updating the Particle Volumes (Equation 15)
        parts.VOL[:,:] = parts.DET_DEFGRAD * parts.RESTVOL

    elif(algorithm_type == TYPE_CPDI2):

        # Updating Particle Positions (Corners and then Centers)
        CORPHI00 = cor00_phiip_cpdi(nodes,parts)
        parts.COR00x[:,:] = parts.COR00x + (CORPHI00[nzmi,:].T).dot(nzmNodeVelx).reshape(partsshape)*dt
        parts.COR00y[:,:] = parts.COR00y + (CORPHI00[nzmi,:].T).dot(nzmNodeVely).reshape(partsshape)*dt
        del CORPHI00

        CORPHI01 = cor01_phiip_cpdi(nodes,parts)
        parts.COR01x[:,:] = parts.COR01x + (CORPHI01[nzmi,:].T).dot(nzmNodeVelx).reshape(partsshape)*dt
        parts.COR01y[:,:] = parts.COR01y + (CORPHI01[nzmi,:].T).dot(nzmNodeVely).reshape(partsshape)*dt
        del CORPHI01

        CORPHI10 = cor10_phiip_cpdi(nodes,parts)
        parts.COR10x[:,:] = parts.COR10x + (CORPHI10[nzmi,:].T).dot(nzmNodeVelx).reshape(partsshape)*dt
        parts.COR10y[:,:] = parts.COR10y + (CORPHI10[nzmi,:].T).dot(nzmNodeVely).reshape(partsshape)*dt
        del CORPHI10

        CORPHI11 = cor11_phiip_cpdi(nodes,parts)
        parts.COR11x[:,:] = parts.COR11x + (CORPHI11[nzmi,:].T).dot(nzmNodeVelx).reshape(partsshape)*dt
        parts.COR11y[:,:] = parts.COR11y + (CORPHI11[nzmi,:].T).dot(nzmNodeVely).reshape(partsshape)*dt
        del CORPHI11

        parts.X = 0.25*(parts.COR00x + parts.COR01x + parts.COR10x + parts.COR11x)
        parts.Y = 0.25*(parts.COR00y + parts.COR01y + parts.COR10y + parts.COR11y)

        # Updating Deformation Gradient (Equation 14)
        # (Doing this explicity from the particle corner locations)

        # Stretched matrix, represnting the stretched configuration of the corners
        # defined by the points: center, middle-of-right-side, and middle-of-top
        stretp00x = parts.X
        stretp00y = parts.Y
        stretp01x = 0.5*(parts.COR01x + parts.COR11x)
        stretp01y = 0.5*(parts.COR01y + parts.COR11y)
        stretp10x = 0.5*(parts.COR10x + parts.COR11x)
        stretp10y = 0.5*(parts.COR10y + parts.COR11y)
        STRETCHED00 = stretp01x-stretp00x
        STRETCHED01 = stretp01y-stretp00y
        STRETCHED10 = stretp10x-stretp00x
        STRETCHED11 = stretp10y-stretp00y

        #Defgrad = STRETCHED * Inverse(REST)
        parts.DEFGRAD00[:,:], parts.DEFGRAD01[:,:], parts.DEFGRAD10[:,:], parts.DEFGRAD11[:,:] = \
                                matrixMultiplyAinvB2x2( ((STRETCHED00,  STRETCHED01 ),(STRETCHED10,  STRETCHED11 )),\
                                                        ((parts.REST00, parts.REST01),(parts.REST10, parts.REST11)) )
        del stretp00x
        del stretp00y
        del stretp01x
        del stretp01y
        del stretp10x
        del stretp10y
        del STRETCHED00
        del STRETCHED01
        del STRETCHED10
        del STRETCHED11

        # Calculating the determinant of the deformation gradient
        parts.DET_DEFGRAD[:,:] = parts.calc_det_defgrad(numpy.where(parts.X)).reshape(partsshape)

        # Updating the Particle Volumes (Equation 15)
        #(TODO: Do this explicity from the particle corner locations)
        parts.VOL[:,:] = parts.DET_DEFGRAD * parts.RESTVOL

    else:
        print("ERROR: 771293 INVALID MPM TYPE")
        quit()

    del nzmi
    del nzmNodeVelx
    del nzmNodeVely

    #-----------------------------------------------------------------
    # Constitutive Model
    #   P = C*K*[exp((1-J)/C)-1]
    #-----------------------------------------------------------------

    nzdfi = numpy.where(numpy.abs(parts.DET_DEFGRAD)>smallvaluelimit)
    negatives, = numpy.where(parts.DET_DEFGRAD.flatten() < 0)

    if(negatives.size > 0):
        print "ERROR! Negative Jacobian!"
        print "negatives.size: ", negatives.size
        for index in xrange(0,negatives.size):
            part_i = parts.I.flatten()[negatives[index]]
            part_j = parts.J.flatten()[negatives[index]]
            print "Particle: ", part_i, part_j," flattened_index: ",negatives[index], \
                  "Location: ", parts.X[part_i,part_j], parts.Y[part_i,part_j], \
                  "det(defgrad): ", parts.DET_DEFGRAD[part_i,part_j]
            print "Corner00: ", parts.COR00x[part_i,part_j], parts.COR00y[part_i,part_j]
            print "Corner01: ", parts.COR01x[part_i,part_j], parts.COR01y[part_i,part_j]
            print "Corner10: ", parts.COR10x[part_i,part_j], parts.COR10y[part_i,part_j]
            print "Corner11: ", parts.COR11x[part_i,part_j], parts.COR11y[part_i,part_j]
        showNodesAndParticles(nodes,parts,numpy.where(nodes.X),numpy.where(parts.X),title="Domain after "+str(step+1)+" steps")
        return False #failure

        # Experiment:  If negative jacobian, reset the defgrad of that particle to rest
        #print "WARNING! Negative Jacobian!"
        #print "negatives.size: ", negatives.size
        #negij = numpy.where(parts.DET_DEFGRAD < 0)
        #parts.DEFGRAD00[negij] = 1.0
        #parts.DEFGRAD01[negij] = 0.0
        #parts.DEFGRAD10[negij] = 0.0
        #parts.DEFGRAD11[negij] = 1.0
        #parts.DET_DEFGRAD[negij] = parts.calc_det_defgrad(negij)
        #parts.COR00x[negij] = parts.X[negij]-0.5*parts.restlengthx
        #parts.COR01x[negij] = parts.X[negij]+0.5*parts.restlengthx
        #parts.COR10x[negij] = parts.X[negij]-0.5*parts.restlengthx
        #parts.COR11x[negij] = parts.X[negij]+0.5*parts.restlengthx
        #parts.COR00y[negij] = parts.X[negij]-0.5*parts.restlengthy
        #parts.COR01y[negij] = parts.X[negij]-0.5*parts.restlengthy
        #parts.COR10y[negij] = parts.X[negij]+0.5*parts.restlengthy
        #parts.COR11y[negij] = parts.X[negij]+0.5*parts.restlengthy




    if(nzdfi[0].size > 0):
        pressure = C_Tait*K_bulkMod*(numpy.exp((1-parts.DET_DEFGRAD[nzdfi])/C_Tait) - 1)
        parts.STRESS_00[nzdfi] = -pressure #+ mu_shear * 2.0 * parts.DELxVx00[nzdfi]
        parts.STRESS_01[nzdfi] = 0         #+ mu_shear * (parts.DELyVx01[nzdfi] + parts.DELxVy10[nzdfi])
        parts.STRESS_10[nzdfi] = 0         #+ mu_shear * (parts.DELxVy10[nzdfi] + parts.DELyVx01[nzdfi])
        parts.STRESS_11[nzdfi] = -pressure #+ mu_shear * 2.0 * parts.DELyVy11[nzdfi]
    else:
        print "  (step ",step,") No nonzero particle jacobians. Constitutitve model skipped."

    return True #success

##END algorithmStep()




#####################
## ANIMATION ####
#####################

step = 0
framescount = 0

def animate(frame):

    global stepsPerFrame, step, framescount, ax, fig, algorithm_type

    for i in xrange(0,stepsPerFrame):
        algorithmStep(step)
        step += 1

    framescount += 1

    print "*** MOVIE FRAME: ",framescount, " ***"

    ax.clear()

    title = algorithm_type+",  "+mapping_type

    theplot = showNodesAndParticles(nodes,parts,numpy.where(nodes.X),numpy.where(parts.X),\
                                  title=algorithm_type+", "+mapping_type+", t = "+str(currenttime)+", step = "+str(step),\
                                  savefig=False,\
                                  newfig=False,\
                                  showCorners=(algorithm_type!=TYPE_MPM),\
                                  ax=ax)
    return theplot


######################
## RUN SIMULATION ####
######################


if(ANIMATE == True):
    fig = pyplot.figure(figsize=(8,7.5))
    ax = fig.add_subplot(111, autoscale_on=False, xlim=(nodes.xmin(), nodes.xmax()), ylim=(nodes.ymin(), nodes.ymax()))

    print "Building Animation..."
    ani = animation.FuncAnimation(fig=fig, func=animate, frames=numpy.arange(1, numFrames+1))
    ani.save(moviefilename, fps=framesPerSecond)
else:
    for step in xrange(0,maxsteps):
        success = algorithmStep(step)
        if(SAVEFIG and step % stepsPerFrame == 0):
            showNodesAndParticles(nodes,parts,numpy.where(nodes.X),numpy.where(parts.X),\
                                  title=algorithm_type+", "+mapping_type+", t = "+str(currenttime)+", step = "+str(step),\
                                  savefig=True, filename="animation/framesf/step"+str(step).zfill(7)+".pdf", \
                                  showCorners=(algorithm_type!=TYPE_MPM))
        if(not success):
            showNodesAndParticles(nodes,parts,numpy.where(nodes.X),numpy.where(parts.DET_DEFGRAD < 0),\
                                  title=algorithm_type+", "+mapping_type+", t = "+str(currenttime)+", step = "+str(step),\
                                  savefig=True, filename="animation/framesf/step9999999negJ.pdf")
            break
            




#####################
## SCRATCH SPACE ####
#####################

# TODO: Particles separating? How do we handle particle separation?

"""
PHI = phiip_mpm(nodes,parts)
PSPHI = linalg.pinv(PHI.dot(PHI.T)).dot(PHI)

nodes.plotNodalShapeFunction(3,4,"regular")
nodes.plotNodalShapeFunction(3,4,"delx")
nodes.plotNodalShapeFunction(3,4,"dely")
showMatrix(PHI,"PHI")
showMatrix(PSPHI,"PSPHI")
showMatrix(PHI.dot(PHI.T),"PHI.dot(PHI.T)")
"""
#ONES = numpy.ones(nodes.X.shape)
#ONES[nodes.pinned_x_bc_indices] = 0
#nodes.plotField(ONES,numpy.nonzero(ONES),title="Partition of Unity",type="wireframe",resolutionPerCell=5)
#nodes.plotField(ONES,numpy.nonzero(ONES),title="Partition of Unity",type="pcolor",resolutionPerCell=5)

showNodesAndParticles(nodes,parts,numpy.where(nodes.X),numpy.where(parts.X),title="Domain after "+str(step+1)+" steps")

nodes.plotField(nodes.M/nodes.VOL, numpy.where(numpy.abs(nodes.VOL)>smallvaluelimit), title="Density",  type="pcolor", resolutionPerCell=5)
nodes.plotField(nodes.Qx/nodes.M, numpy.where(numpy.abs(nodes.M)>smallvaluelimit), title="Vx", type="pcolor", resolutionPerCell=5)
nodes.plotField(nodes.Qy/nodes.M, numpy.where(numpy.abs(nodes.M)>smallvaluelimit), title="Vy", type="pcolor", resolutionPerCell=5)



